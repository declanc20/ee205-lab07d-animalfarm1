///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm 1 - EE 205 - Spr 2022
///
/// Usage: defines program names 
///
///
/// @file config.h
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/28/22
///////////////////////////////////////////////////////////////////////////////

#pragma once

#define PROGRAM_NAME "animal farm 1"
