////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - animal farm1 - EE 205 - Spr 2022
///
/// @file addCat.h
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 28_Feb_2022
////////////////////////////////////////////////////////////////////////////

#pragma once

bool addCat(const char name[], const enum Gender gender, const enum Breed breed, const bool isfixed, const float weight, const enum Color collar1, const enum Color collar2,               const unsigned long long license );

