///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm 1 - EE 205 - Spr 2022
///
/// Usage: 
///
///
/// @file catDataBase.h
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/28/22
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include<stdio.h>
#include<stdbool.h>

#define MAX_CATS (1024)
#define MAX_NAME (50)
#define FALSE 0
#define TRUE 1


 enum Gender{ UNKNOWN_GENDER, MALE, FEMALE };
 enum Breed{ UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX}; 
 enum Color{ BLACK, WHITE, RED, BLUE, GREEN, PINK};

struct Cat {  //database is 
   char                 name[MAX_NAME];
   enum                 Gender gender;
   enum                 Breed breed;
   bool                 isFixed;
   float                weight; 
   enum                 Color collarColor1;
   enum                 Color collarColor2;
   unsigned long long   license;
   };  

extern struct Cat dataBase[MAX_CATS];

extern int numOfCats; //global variable for number of cats in database
