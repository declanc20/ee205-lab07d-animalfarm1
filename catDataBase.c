///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm 1 - EE 205 - Spr 2022
///
/// Usage: Cat Data base  
///
///
/// @file @catDataBase.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/28/22
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include "catDataBase.h"

struct Cat dataBase[MAX_CATS];
int numOfCats = 0;

