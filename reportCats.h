///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm 1 - EE 205 - Spr 2022
///
///
/// @file reportCats.h
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/28/22
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern bool findCat( char name[]);
extern void printAllCats(void);
extern bool printCat( int catNum);

