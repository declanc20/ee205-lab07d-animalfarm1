###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - animalfarm1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
###
### @author Declan Campbell <declanc@hawaii.edu>
### @date 28_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC = gcc
CFLAGS = -g -Wall -Wextra 

TARGET = animalFarm

all: $(TARGET)

addCats.o: addCats.c addCats.h catDataBase.h config.h
	$(CC) $(CFLAGS) -c addCats.c
deleteCats.o: deleteCats.c deleteCats.h catDataBase.h 
	$(CC) $(CFLAGS) -c deleteCats.c
reportCats.o: reportCats.c reportCats.h catDataBase.h config.h convertenum.h
	$(CC) $(CFLAGS) -c reportCats.c
updateCats.o: updateCats.c updateCats.h catDataBase.h config.h
	$(CC) $(CFLAGS) -c updateCats.c
convertenum.o: convertenum.c convertenum.h catDataBase.h
	$(CC) $(CFLAGS) -c convertenum.c
main.o: main.c addCats.h deleteCats.h reportCats.h updateCats.h catDataBase.h convertenum.o
	$(CC) $(CFLAGS) -c main.c

$(TARGET): main.o catDataBase.o addCats.o deleteCats.o reportCats.o updateCats.o convertenum.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDataBase.o addCats.o deleteCats.o reportCats.o updateCats.o convertenum.o


test: animalFarm
	./animalFarm

clean:
	rm -f $(TARGET) *.o
