///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - animalfarm 1 - EE 205 - Spr 2022
///
///
///
/// @file convertenum.h
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   3/01/22
///////////////////////////////////////////////////////////////////////////////

#pragma once

extern char* colorName(const enum Color color);
extern char* breedName(const enum Breed breed); 
extern char* genderName(const enum Gender gender);
